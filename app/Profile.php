<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
	protected $guarded = [];

	public function profileImage()
	{
		return ($this->image) ? '/storage/' . $this->image : 'https://cdn.pixabay.com/photo/2018/05/28/22/11/message-in-a-bottle-3437294__340.jpg';
	}

	public function followers()
    {
        return $this->belongsToMany(User::class);
    }
	
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
